import { useQuery } from 'react-query';

import { api } from '../api/api';

export const useFetchWeather = () => {
    const query = useQuery('user', api.getWeather);

    return query;
};
