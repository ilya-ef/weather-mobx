
import { observer } from 'mobx-react-lite';
import { store } from '../lib/mobx/weatherStore';

// Helpers
import { formatDate } from '../helpers/formatDate';

export const WeekDay = observer((props) => {
    const {
        id,
        temperature,
        day,
        type,
    } = props;

    return (
        <div
            className = { `day ${type} ${store.selectedDay.id === id ? 'selected' : ''} ` }
            onClick = { () => store.setSelectedDay(props) }>
            <p>{ formatDate(day).formattedWeekDay }</p>
            <span>{ temperature }</span>
        </div>
    );
});
