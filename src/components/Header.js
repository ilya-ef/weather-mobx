// Mobx
import { observer } from 'mobx-react-lite';

// Helpers
import { formatDate } from '../helpers/formatDate';

// Store
import { store } from '../lib/mobx/weatherStore';


export const Header = observer(() => {
    return (
        store.filterIncludesData && <div className = 'head'>
            <div className = { `icon ${store.selectedDay.type}` }></div>
            <div className = 'current-date'>
                <p>{ formatDate(store.selectedDay.day).formattedWeekDay }</p>
                <span>{ formatDate(store.selectedDay.day).formattedMonth } </span>
            </div>
        </div>
    );
});
