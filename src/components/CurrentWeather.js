// Mobx
import { observer } from 'mobx-react-lite';

// Store
import { store } from '../lib/mobx/weatherStore';

export const CurrentWeather = observer(() => {
    return (
        store.filterIncludesData && <div className = 'current-weather'>
            <p className = 'temperature'>{ store.selectedDay.temperature }</p>
            <p className = 'meta'>
                <span className = 'rainy'>{ `%${store.selectedDay.rain_probability}` }</span>
                <span className = 'humidity'>{ `%${store.selectedDay.humidity}` }</span>
            </p>
        </div>
    );
});
