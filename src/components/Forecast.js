// Mobx
import { observer } from 'mobx-react-lite';

import { WeekDay } from './WeekDay';

// Store
import { store } from '../lib/mobx/weatherStore';

export const Forecast = observer((props) => {
    const { days } = props;
    let weekDayJSX;

    if (store.isFiltered) {
        if (store.filteredDays(days).length) {
            weekDayJSX = store.filteredDays(days).slice(0, 7).map((day) => {
                return <WeekDay key = { day.id } { ...day } />;
            });
        } else {
            weekDayJSX = <p className = 'message'>По заданным критериям нет доступных дней!</p>;
        }
    } else {
        weekDayJSX = days.slice(0, 7).map((day) => {
            return <WeekDay key = { day.id } { ...day } />;
        });
    }

    return (
        <div className = 'forecast'>
            { weekDayJSX }
        </div>
    );
});
