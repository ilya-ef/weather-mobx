// Mobx
import { action } from 'mobx';
import { observer } from 'mobx-react-lite';

// Core
import { useForm } from 'react-hook-form';
import { useEffect } from 'react';

// Store
import { store } from '../lib/mobx/weatherStore';


export const Filter = observer((props) => {
    const { days } = props;

    const form = useForm({
        mode:          'onTouched',
        defaultValues: {
            minTemperature: '',
            maxTemperature: '',
        },
    });

    const clearCheckboxesPrevState = () => {
        const checkboxes = document.querySelectorAll('.checkbox');
        checkboxes.forEach((checkbox) => checkbox.classList.remove('selected'));
    };

    const handleChecked = (type, checkbox) => {
        clearCheckboxesPrevState();
        checkbox.classList.add('selected');

        store.setType(type);
    };

    const onSubmit = form.handleSubmit(action((data) => {
        if (store.isFiltered) {
            form.reset();
            store.resetFilter();
            clearCheckboxesPrevState();
            store.setFilterIncludesData(true);
            store.setSelectedDay(days[ 0 ]);

            return;
        }

        // eslint-disable-next-line no-unused-expressions
        store.filteredDays(days).length
            ? store.setSelectedDay(store.filteredDays(days)[ 0 ])
            : store.setFilterIncludesData(false);

        const { maxTemperature, minTemperature } = data;
        store.applyFilter({ type: store.type, minTemperature, maxTemperature });
    }));

    return (
        <form onSubmit = { onSubmit }>
            <div className = 'filter'>
                <span
                    style = { store.isFiltered ? { pointerEvents: 'none' } : {} }
                    className = 'checkbox'
                    onClick = { (event) => handleChecked('cloudy', event.currentTarget) }>Облачно</span>

                <span
                    style = { store.isFiltered ? { pointerEvents: 'none' } : {} }
                    className = 'checkbox'
                    onClick = { (event) => handleChecked('sunny', event.currentTarget) }>Солнечно</span>

                <p className = 'custom-input'>
                    <label htmlFor = 'min-temperature'>Минимальная температура</label>
                    <input
                        id = 'min-temperature'
                        type = 'number'
                        onInput = { (event) => store.setMinTemperature(event.currentTarget.value) }
                        disabled = { store.isFiltered }
                        { ...form.register('minTemperature') } />
                </p>
                <p className = 'custom-input'>
                    <label htmlFor = 'max-temperature'>Максимальная температура</label>
                    <input
                        id = 'max-temperature'
                        type = 'number'
                        onInput = { (event) => store.setMaxTemperature(event.currentTarget.value) }
                        disabled = { store.isFiltered }
                        { ...form.register('maxTemperature') } />
                </p>
                <button type = 'submit' disabled = { !store.isFormEnabled }>{ store.isFiltered ? 'Сбросить' : 'Отфильтровать' }</button>
            </div>
        </form>
    );
});
