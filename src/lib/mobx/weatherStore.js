// Core
import { makeAutoObservable, runInAction } from 'mobx';
import { computedFn } from 'mobx-utils';

export class WeatherStore {
    type = '';
    minTemperature = '';
    maxTemperature = '';
    isFiltered = false;
    filterIncludesData = true;
    selectedDay = '';

    constructor() {
        this
            .filteredDays = computedFn((days) => {
                const filteredDays = days.filter((day) => {
                    const isCorrectType = this.type
                        ? this.type === day.type
                        : true;
                    const isCorrectMinTemperature = this.minTemperature
                        ? Number(this.minTemperature) <= day.temperature
                        : true;
                    const isCorrectMaxTemperature = this.maxTemperature
                        ? Number(this.maxTemperature) >= day.temperature
                        : true;

                    return (
                        isCorrectType
                    && isCorrectMinTemperature
                    && isCorrectMaxTemperature
                    );
                });

                return filteredDays;
            });

        makeAutoObservable(this, {}, { autoBind: true });
    }

    setType(type) {
        this.type = type;
    }

    setMinTemperature(temp) {
        this.minTemperature = temp;
    }

    setMaxTemperature(temp) {
        this.maxTemperature = temp;
    }

    applyFilter(filter) {
        if (filter.type) {
            this.type = filter.type;
        }

        if (filter.minTemperature) {
            this.minTemperature = filter.minTemperature;
        }

        if (filter.maxTemperature) {
            this.maxTemperature = filter.maxTemperature;
        }

        this.isFiltered = true;
    }

    get isFormEnabled() {
        return this.type || this.maxTemperature || this.minTemperature;
    }

    setSelectedDay(day) {
        this.selectedDay = day;
    }

    setFilterIncludesData(isIncludesData) {
        this.filterIncludesData = isIncludesData;
    }

    resetFilter() {
        this.maxTemperature = '';
        this.minTemperature = '';
        this.type = '';
        this.isFiltered = false;
    }
}

export const store = new WeatherStore();
