import { observer } from 'mobx-react-lite';
import { isToday } from 'date-fns';

// Components
import {
    Header, Filter, Forecast, CurrentWeather,
} from './components';

// Hooks
import { useFetchWeather } from './hooks/useFetchWeather';

// Store
import { store } from './lib/mobx/weatherStore';

export const App = () => {
    const { isFetched, data } = useFetchWeather();

    if (isFetched) {
        const currentDateWeather = data.length && data.find((item) => isToday(item.day));
        store.setSelectedDay(currentDateWeather);
    }

    return (
        isFetched && <main>
            <Filter days = { data } />
            <Header />
            <CurrentWeather />
            <Forecast days = { data } />
        </main>
    );
};

