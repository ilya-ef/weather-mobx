import { format } from 'date-fns';
import { ru } from 'date-fns/locale';

export const formatDate = (date) => {
    if (!date) {
        return null;
    }

    const formattedDate = {
        formattedWeekDay: format(new Date(date), 'EEEE', { locale: ru }),
        formattedMonth:   format(new Date(date), 'dd MMMM', { locale: ru }),
    };

    return formattedDate;
};
